raw_info.name = $('.details-title-eng').text().trim();
raw_info.small_descr = $('.details-title-chs').text().trim().replace(/【|】/g, '') + ' ' + $('.details-title-one-sentence').text().trim();
raw_info.url = match_link('imdb', $('#main').html());
$('.details-download-section').after(`<div class="details-info-section"><table id="HDRTable"><tbody></tbody></table></div>`);
table = document.getElementById('HDRTable');
insert_row = table.insertRow(0);
tbody = table.getElementsByTagName('tbody')[0];

descr = document.getElementsByClassName('details-description-section')[0].cloneNode(true);
raw_info.descr = `[img]${$('.details-poster-section img').prop('src')}[/img]\n\n` + walkDOM(descr).trim();
raw_info.descr = raw_info.descr.replace(/(\[\/quote\])/, "$1\n").replace(/(\[quote\])/, "\n$1");

raw_info.type = '电影';
raw_info.medium_sel = 'Blu-ray';
no_need_douban_button_sites.push('HDRoute');